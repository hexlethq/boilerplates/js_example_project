FROM node:14.5.0-alpine3.12

RUN apk update
RUN apk add --update-cache bash make

WORKDIR /project
# RUN mkdir /project/code

# COPY code/package.json code
# COPY code/package-lock.json code
# RUN cd code && npm ci

# COPY code ./code

# COPY package.json .
# COPY package-lock.json .
# RUN npm ci

# COPY Makefile .
# COPY .eslintrc.yml .

# COPY __tests__ __tests__
# COPY __fixtures__ __fixtures__

COPY . .
