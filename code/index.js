import fastify from 'fastify';

const app = fastify({ logger: true });

app.get('/', async () => {
  const data = { hello: 'world' };
  return data;
});

export default app;
