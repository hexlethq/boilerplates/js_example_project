ci:
	docker-compose -f docker-compose.yml run app make setup
	docker-compose -f docker-compose.yml up --abort-on-container-exit

compose-setup: compose-build compose-setup

compose-build:
	docker-compose build

compose-setup:
	docker-compose run app make setup

compose-bash:
	docker-compose run app bash

compose-lint:
	docker-compose run test make lint

compose-test:
	docker-compose -f docker-compose.yml up --abort-on-container-exit

compose-publish:
	docker-compose run app make publish

setup:
	npm install

test:
	npm -s test

lint:
	npx eslint --no-eslintrc --config .eslintrc.yml .
