import app from '@hexlet/code';

test('app', async () => {
  const response = await app.inject({
    method: 'GET',
    url: '/',
  });
  expect(response).toHaveProperty('statusCode', 200);
});
